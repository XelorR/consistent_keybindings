SetCapsLockState "AlwaysOff"

#HotIf WinActive("ahk_class Emacs")
{
    CapsLock::Control
    ;CapsLock & Space::Send "{Alt Down}{Shift Down}{Shift Up}{Alt Up}"
    #m::F11
    #d::#Tab
    #Space::#s
    #Tab::!Tab
}

#HotIf WinActive("ahk_class Chrome_WidgetWin_1")
{
    ; browser
    ;#\{::^PgUp
    ;#\}::^PgDn
    #r::^r
    #w::^w
    #l::^l
    #t::^t
    #+t::^+t
    #Left::!Left
    #Right::!Right
}

#HotIf not WinActive("ahk_class Emacs")
{
    CapsLock & Space::Send "{Alt Down}{Shift Down}{Shift Up}{Alt Up}"

    ; system
    #LButton::^LButton
    #Enter::^Enter
    #Space::#s
    #a::^a
    #c::^c
    #x::^x
    #z::^z
    #u::^u
    #k::^k
    #v::^v    
    #b::^b
    #i::^i
    #s::^s
    #o::^o
    #f::^f
    #h::^h
    #n::^n
    #+n::^+n
    #+x::PrintScreen
    #d::#Tab
    #Tab::!Tab
    #^q::#l
    #m::F11
    #^f::F11
    #q::!F4

    ; emacs
    !b::^Left
    !f::^Right
    !Left::^Left
    !Right::^Right
    !d::^Del
    !Backspace::^Backspace
    !w::^c
    !v::PgUp
    !+,::^Home
    !+.::^End
    ^v::PgDn
    ^w::^x
    ^d::Del
    ^p::Up
    ^n::Down
    ^b::Left
    ^f::Right
    ^a::Home
    ^e::End
    ^m::Enter
    ^j::Enter
    ^o::Send "{Enter}{Left}"
    ^/::^z
    ^y::^v
    ^g::Esc
    ^s::F3
    ^r::Send "{Shift Down}{F3}{Shift Up}"
    ^k::Send "{Shift Down}{End}{Shift Up}{Del}"
    ^u::Send "{Shift Down}{Home}{Shift Up}{Del}"

    CapsLock & v::PgDn
    CapsLock & w::^x
    CapsLock & d::Del
    CapsLock & p::Up
    CapsLock & n::Down
    CapsLock & b::Left
    CapsLock & f::Right
    CapsLock & a::Home
    CapsLock & e::End
    CapsLock & m::Enter
    CapsLock & j::Enter
    CapsLock & o::Send "{Enter}{Left}"
    CapsLock & /::^z
    CapsLock & y::^v
    CapsLock & g::Esc
    CapsLock & s::F3
    CapsLock & r::Send "{Shift Down}{F3}{Shift Up}"
    CapsLock & k::Send "{Shift Down}{End}{Shift Up}{Del}"
    CapsLock & u::Send "{Shift Down}{Home}{Shift Up}{Del}"
}

#HotIf WinActive("ahk_exe powershell.exe")
{
    #r::^r
}

#HotIf WinActive("ahk_exe OUTLOOK.EXE")
{
    #r::^r
    #^r::^+r
    #f::^f
    #1::^1
    #2::^2
    #e::^e
    #n::^n
    #+n::^+n
    #.::^.
    #,::^,
}
